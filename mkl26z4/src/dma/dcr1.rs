#[doc = "Reader of register DCR1"]
pub type R = crate::R<u32, super::DCR1>;
#[doc = "Writer for register DCR1"]
pub type W = crate::W<u32, super::DCR1>;
#[doc = "Register DCR1 `reset()`'s with value 0"]
impl crate::ResetValue for super::DCR1 {
    type Type = u32;
    #[inline(always)]
    fn reset_value() -> Self::Type {
        0
    }
}
#[doc = "Possible values of the field `LCH2`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LCH2_A {
    #[doc = "DMA Channel 0"]
    _00,
    #[doc = "DMA Channel 1"]
    _01,
    #[doc = "DMA Channel 2"]
    _10,
    #[doc = "DMA Channel 3"]
    _11,
}
impl crate::ToBits<u8> for LCH2_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            LCH2_A::_00 => 0,
            LCH2_A::_01 => 1,
            LCH2_A::_10 => 2,
            LCH2_A::_11 => 3,
        }
    }
}
#[doc = "Reader of field `LCH2`"]
pub type LCH2_R = crate::R<u8, LCH2_A>;
impl LCH2_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LCH2_A {
        match self.bits {
            0 => LCH2_A::_00,
            1 => LCH2_A::_01,
            2 => LCH2_A::_10,
            3 => LCH2_A::_11,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_00`"]
    #[inline(always)]
    pub fn is_00(&self) -> bool {
        *self == LCH2_A::_00
    }
    #[doc = "Checks if the value of the field is `_01`"]
    #[inline(always)]
    pub fn is_01(&self) -> bool {
        *self == LCH2_A::_01
    }
    #[doc = "Checks if the value of the field is `_10`"]
    #[inline(always)]
    pub fn is_10(&self) -> bool {
        *self == LCH2_A::_10
    }
    #[doc = "Checks if the value of the field is `_11`"]
    #[inline(always)]
    pub fn is_11(&self) -> bool {
        *self == LCH2_A::_11
    }
}
#[doc = "Write proxy for field `LCH2`"]
pub struct LCH2_W<'a> {
    w: &'a mut W,
}
impl<'a> LCH2_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: LCH2_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bits(variant._bits())
        }
    }
    #[doc = "DMA Channel 0"]
    #[inline(always)]
    pub fn _00(self) -> &'a mut W {
        self.variant(LCH2_A::_00)
    }
    #[doc = "DMA Channel 1"]
    #[inline(always)]
    pub fn _01(self) -> &'a mut W {
        self.variant(LCH2_A::_01)
    }
    #[doc = "DMA Channel 2"]
    #[inline(always)]
    pub fn _10(self) -> &'a mut W {
        self.variant(LCH2_A::_10)
    }
    #[doc = "DMA Channel 3"]
    #[inline(always)]
    pub fn _11(self) -> &'a mut W {
        self.variant(LCH2_A::_11)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bits(self, value: u8) -> &'a mut W {
        self.w.bits = (self.w.bits & !0x03) | ((value as u32) & 0x03);
        self.w
    }
}
#[doc = "Possible values of the field `LCH1`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LCH1_A {
    #[doc = "DMA Channel 0"]
    _00,
    #[doc = "DMA Channel 1"]
    _01,
    #[doc = "DMA Channel 2"]
    _10,
    #[doc = "DMA Channel 3"]
    _11,
}
impl crate::ToBits<u8> for LCH1_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            LCH1_A::_00 => 0,
            LCH1_A::_01 => 1,
            LCH1_A::_10 => 2,
            LCH1_A::_11 => 3,
        }
    }
}
#[doc = "Reader of field `LCH1`"]
pub type LCH1_R = crate::R<u8, LCH1_A>;
impl LCH1_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LCH1_A {
        match self.bits {
            0 => LCH1_A::_00,
            1 => LCH1_A::_01,
            2 => LCH1_A::_10,
            3 => LCH1_A::_11,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_00`"]
    #[inline(always)]
    pub fn is_00(&self) -> bool {
        *self == LCH1_A::_00
    }
    #[doc = "Checks if the value of the field is `_01`"]
    #[inline(always)]
    pub fn is_01(&self) -> bool {
        *self == LCH1_A::_01
    }
    #[doc = "Checks if the value of the field is `_10`"]
    #[inline(always)]
    pub fn is_10(&self) -> bool {
        *self == LCH1_A::_10
    }
    #[doc = "Checks if the value of the field is `_11`"]
    #[inline(always)]
    pub fn is_11(&self) -> bool {
        *self == LCH1_A::_11
    }
}
#[doc = "Write proxy for field `LCH1`"]
pub struct LCH1_W<'a> {
    w: &'a mut W,
}
impl<'a> LCH1_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: LCH1_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bits(variant._bits())
        }
    }
    #[doc = "DMA Channel 0"]
    #[inline(always)]
    pub fn _00(self) -> &'a mut W {
        self.variant(LCH1_A::_00)
    }
    #[doc = "DMA Channel 1"]
    #[inline(always)]
    pub fn _01(self) -> &'a mut W {
        self.variant(LCH1_A::_01)
    }
    #[doc = "DMA Channel 2"]
    #[inline(always)]
    pub fn _10(self) -> &'a mut W {
        self.variant(LCH1_A::_10)
    }
    #[doc = "DMA Channel 3"]
    #[inline(always)]
    pub fn _11(self) -> &'a mut W {
        self.variant(LCH1_A::_11)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bits(self, value: u8) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x03 << 2)) | (((value as u32) & 0x03) << 2);
        self.w
    }
}
#[doc = "Possible values of the field `LINKCC`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LINKCC_A {
    #[doc = "No channel-to-channel linking"]
    _00,
    #[doc = "Perform a link to channel LCH1 after each cycle-steal transfer followed by a link to LCH2 after the BCR decrements to 0."]
    _01,
    #[doc = "Perform a link to channel LCH1 after each cycle-steal transfer"]
    _10,
    #[doc = "Perform a link to channel LCH1 after the BCR decrements to 0."]
    _11,
}
impl crate::ToBits<u8> for LINKCC_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            LINKCC_A::_00 => 0,
            LINKCC_A::_01 => 1,
            LINKCC_A::_10 => 2,
            LINKCC_A::_11 => 3,
        }
    }
}
#[doc = "Reader of field `LINKCC`"]
pub type LINKCC_R = crate::R<u8, LINKCC_A>;
impl LINKCC_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LINKCC_A {
        match self.bits {
            0 => LINKCC_A::_00,
            1 => LINKCC_A::_01,
            2 => LINKCC_A::_10,
            3 => LINKCC_A::_11,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_00`"]
    #[inline(always)]
    pub fn is_00(&self) -> bool {
        *self == LINKCC_A::_00
    }
    #[doc = "Checks if the value of the field is `_01`"]
    #[inline(always)]
    pub fn is_01(&self) -> bool {
        *self == LINKCC_A::_01
    }
    #[doc = "Checks if the value of the field is `_10`"]
    #[inline(always)]
    pub fn is_10(&self) -> bool {
        *self == LINKCC_A::_10
    }
    #[doc = "Checks if the value of the field is `_11`"]
    #[inline(always)]
    pub fn is_11(&self) -> bool {
        *self == LINKCC_A::_11
    }
}
#[doc = "Write proxy for field `LINKCC`"]
pub struct LINKCC_W<'a> {
    w: &'a mut W,
}
impl<'a> LINKCC_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: LINKCC_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bits(variant._bits())
        }
    }
    #[doc = "No channel-to-channel linking"]
    #[inline(always)]
    pub fn _00(self) -> &'a mut W {
        self.variant(LINKCC_A::_00)
    }
    #[doc = "Perform a link to channel LCH1 after each cycle-steal transfer followed by a link to LCH2 after the BCR decrements to 0."]
    #[inline(always)]
    pub fn _01(self) -> &'a mut W {
        self.variant(LINKCC_A::_01)
    }
    #[doc = "Perform a link to channel LCH1 after each cycle-steal transfer"]
    #[inline(always)]
    pub fn _10(self) -> &'a mut W {
        self.variant(LINKCC_A::_10)
    }
    #[doc = "Perform a link to channel LCH1 after the BCR decrements to 0."]
    #[inline(always)]
    pub fn _11(self) -> &'a mut W {
        self.variant(LINKCC_A::_11)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bits(self, value: u8) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x03 << 4)) | (((value as u32) & 0x03) << 4);
        self.w
    }
}
#[doc = "Possible values of the field `D_REQ`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum D_REQ_A {
    #[doc = "ERQ bit is not affected."]
    _0,
    #[doc = "ERQ bit is cleared when the BCR is exhausted."]
    _1,
}
impl crate::ToBits<bool> for D_REQ_A {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            D_REQ_A::_0 => false,
            D_REQ_A::_1 => true,
        }
    }
}
#[doc = "Reader of field `D_REQ`"]
pub type D_REQ_R = crate::R<bool, D_REQ_A>;
impl D_REQ_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> D_REQ_A {
        match self.bits {
            false => D_REQ_A::_0,
            true => D_REQ_A::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == D_REQ_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == D_REQ_A::_1
    }
}
#[doc = "Write proxy for field `D_REQ`"]
pub struct D_REQ_W<'a> {
    w: &'a mut W,
}
impl<'a> D_REQ_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: D_REQ_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "ERQ bit is not affected."]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(D_REQ_A::_0)
    }
    #[doc = "ERQ bit is cleared when the BCR is exhausted."]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(D_REQ_A::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 7)) | (((value as u32) & 0x01) << 7);
        self.w
    }
}
#[doc = "Possible values of the field `DMOD`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum DMOD_A {
    #[doc = "Buffer disabled"]
    _0000,
    #[doc = "Circular buffer size is 16 bytes"]
    _0001,
    #[doc = "Circular buffer size is 32 bytes"]
    _0010,
    #[doc = "Circular buffer size is 64 bytes"]
    _0011,
    #[doc = "Circular buffer size is 128 bytes"]
    _0100,
    #[doc = "Circular buffer size is 256 bytes"]
    _0101,
    #[doc = "Circular buffer size is 512 bytes"]
    _0110,
    #[doc = "Circular buffer size is 1 KB"]
    _0111,
    #[doc = "Circular buffer size is 2 KB"]
    _1000,
    #[doc = "Circular buffer size is 4 KB"]
    _1001,
    #[doc = "Circular buffer size is 8 KB"]
    _1010,
    #[doc = "Circular buffer size is 16 KB"]
    _1011,
    #[doc = "Circular buffer size is 32 KB"]
    _1100,
    #[doc = "Circular buffer size is 64 KB"]
    _1101,
    #[doc = "Circular buffer size is 128 KB"]
    _1110,
    #[doc = "Circular buffer size is 256 KB"]
    _1111,
}
impl crate::ToBits<u8> for DMOD_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            DMOD_A::_0000 => 0,
            DMOD_A::_0001 => 1,
            DMOD_A::_0010 => 2,
            DMOD_A::_0011 => 3,
            DMOD_A::_0100 => 4,
            DMOD_A::_0101 => 5,
            DMOD_A::_0110 => 6,
            DMOD_A::_0111 => 7,
            DMOD_A::_1000 => 8,
            DMOD_A::_1001 => 9,
            DMOD_A::_1010 => 10,
            DMOD_A::_1011 => 11,
            DMOD_A::_1100 => 12,
            DMOD_A::_1101 => 13,
            DMOD_A::_1110 => 14,
            DMOD_A::_1111 => 15,
        }
    }
}
#[doc = "Reader of field `DMOD`"]
pub type DMOD_R = crate::R<u8, DMOD_A>;
impl DMOD_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DMOD_A {
        match self.bits {
            0 => DMOD_A::_0000,
            1 => DMOD_A::_0001,
            2 => DMOD_A::_0010,
            3 => DMOD_A::_0011,
            4 => DMOD_A::_0100,
            5 => DMOD_A::_0101,
            6 => DMOD_A::_0110,
            7 => DMOD_A::_0111,
            8 => DMOD_A::_1000,
            9 => DMOD_A::_1001,
            10 => DMOD_A::_1010,
            11 => DMOD_A::_1011,
            12 => DMOD_A::_1100,
            13 => DMOD_A::_1101,
            14 => DMOD_A::_1110,
            15 => DMOD_A::_1111,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_0000`"]
    #[inline(always)]
    pub fn is_0000(&self) -> bool {
        *self == DMOD_A::_0000
    }
    #[doc = "Checks if the value of the field is `_0001`"]
    #[inline(always)]
    pub fn is_0001(&self) -> bool {
        *self == DMOD_A::_0001
    }
    #[doc = "Checks if the value of the field is `_0010`"]
    #[inline(always)]
    pub fn is_0010(&self) -> bool {
        *self == DMOD_A::_0010
    }
    #[doc = "Checks if the value of the field is `_0011`"]
    #[inline(always)]
    pub fn is_0011(&self) -> bool {
        *self == DMOD_A::_0011
    }
    #[doc = "Checks if the value of the field is `_0100`"]
    #[inline(always)]
    pub fn is_0100(&self) -> bool {
        *self == DMOD_A::_0100
    }
    #[doc = "Checks if the value of the field is `_0101`"]
    #[inline(always)]
    pub fn is_0101(&self) -> bool {
        *self == DMOD_A::_0101
    }
    #[doc = "Checks if the value of the field is `_0110`"]
    #[inline(always)]
    pub fn is_0110(&self) -> bool {
        *self == DMOD_A::_0110
    }
    #[doc = "Checks if the value of the field is `_0111`"]
    #[inline(always)]
    pub fn is_0111(&self) -> bool {
        *self == DMOD_A::_0111
    }
    #[doc = "Checks if the value of the field is `_1000`"]
    #[inline(always)]
    pub fn is_1000(&self) -> bool {
        *self == DMOD_A::_1000
    }
    #[doc = "Checks if the value of the field is `_1001`"]
    #[inline(always)]
    pub fn is_1001(&self) -> bool {
        *self == DMOD_A::_1001
    }
    #[doc = "Checks if the value of the field is `_1010`"]
    #[inline(always)]
    pub fn is_1010(&self) -> bool {
        *self == DMOD_A::_1010
    }
    #[doc = "Checks if the value of the field is `_1011`"]
    #[inline(always)]
    pub fn is_1011(&self) -> bool {
        *self == DMOD_A::_1011
    }
    #[doc = "Checks if the value of the field is `_1100`"]
    #[inline(always)]
    pub fn is_1100(&self) -> bool {
        *self == DMOD_A::_1100
    }
    #[doc = "Checks if the value of the field is `_1101`"]
    #[inline(always)]
    pub fn is_1101(&self) -> bool {
        *self == DMOD_A::_1101
    }
    #[doc = "Checks if the value of the field is `_1110`"]
    #[inline(always)]
    pub fn is_1110(&self) -> bool {
        *self == DMOD_A::_1110
    }
    #[doc = "Checks if the value of the field is `_1111`"]
    #[inline(always)]
    pub fn is_1111(&self) -> bool {
        *self == DMOD_A::_1111
    }
}
#[doc = "Write proxy for field `DMOD`"]
pub struct DMOD_W<'a> {
    w: &'a mut W,
}
impl<'a> DMOD_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: DMOD_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bits(variant._bits())
        }
    }
    #[doc = "Buffer disabled"]
    #[inline(always)]
    pub fn _0000(self) -> &'a mut W {
        self.variant(DMOD_A::_0000)
    }
    #[doc = "Circular buffer size is 16 bytes"]
    #[inline(always)]
    pub fn _0001(self) -> &'a mut W {
        self.variant(DMOD_A::_0001)
    }
    #[doc = "Circular buffer size is 32 bytes"]
    #[inline(always)]
    pub fn _0010(self) -> &'a mut W {
        self.variant(DMOD_A::_0010)
    }
    #[doc = "Circular buffer size is 64 bytes"]
    #[inline(always)]
    pub fn _0011(self) -> &'a mut W {
        self.variant(DMOD_A::_0011)
    }
    #[doc = "Circular buffer size is 128 bytes"]
    #[inline(always)]
    pub fn _0100(self) -> &'a mut W {
        self.variant(DMOD_A::_0100)
    }
    #[doc = "Circular buffer size is 256 bytes"]
    #[inline(always)]
    pub fn _0101(self) -> &'a mut W {
        self.variant(DMOD_A::_0101)
    }
    #[doc = "Circular buffer size is 512 bytes"]
    #[inline(always)]
    pub fn _0110(self) -> &'a mut W {
        self.variant(DMOD_A::_0110)
    }
    #[doc = "Circular buffer size is 1 KB"]
    #[inline(always)]
    pub fn _0111(self) -> &'a mut W {
        self.variant(DMOD_A::_0111)
    }
    #[doc = "Circular buffer size is 2 KB"]
    #[inline(always)]
    pub fn _1000(self) -> &'a mut W {
        self.variant(DMOD_A::_1000)
    }
    #[doc = "Circular buffer size is 4 KB"]
    #[inline(always)]
    pub fn _1001(self) -> &'a mut W {
        self.variant(DMOD_A::_1001)
    }
    #[doc = "Circular buffer size is 8 KB"]
    #[inline(always)]
    pub fn _1010(self) -> &'a mut W {
        self.variant(DMOD_A::_1010)
    }
    #[doc = "Circular buffer size is 16 KB"]
    #[inline(always)]
    pub fn _1011(self) -> &'a mut W {
        self.variant(DMOD_A::_1011)
    }
    #[doc = "Circular buffer size is 32 KB"]
    #[inline(always)]
    pub fn _1100(self) -> &'a mut W {
        self.variant(DMOD_A::_1100)
    }
    #[doc = "Circular buffer size is 64 KB"]
    #[inline(always)]
    pub fn _1101(self) -> &'a mut W {
        self.variant(DMOD_A::_1101)
    }
    #[doc = "Circular buffer size is 128 KB"]
    #[inline(always)]
    pub fn _1110(self) -> &'a mut W {
        self.variant(DMOD_A::_1110)
    }
    #[doc = "Circular buffer size is 256 KB"]
    #[inline(always)]
    pub fn _1111(self) -> &'a mut W {
        self.variant(DMOD_A::_1111)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bits(self, value: u8) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x0f << 8)) | (((value as u32) & 0x0f) << 8);
        self.w
    }
}
#[doc = "Possible values of the field `SMOD`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum SMOD_A {
    #[doc = "Buffer disabled"]
    _0000,
    #[doc = "Circular buffer size is 16 bytes."]
    _0001,
    #[doc = "Circular buffer size is 32 bytes."]
    _0010,
    #[doc = "Circular buffer size is 64 bytes."]
    _0011,
    #[doc = "Circular buffer size is 128 bytes."]
    _0100,
    #[doc = "Circular buffer size is 256 bytes."]
    _0101,
    #[doc = "Circular buffer size is 512 bytes."]
    _0110,
    #[doc = "Circular buffer size is 1 KB."]
    _0111,
    #[doc = "Circular buffer size is 2 KB."]
    _1000,
    #[doc = "Circular buffer size is 4 KB."]
    _1001,
    #[doc = "Circular buffer size is 8 KB."]
    _1010,
    #[doc = "Circular buffer size is 16 KB."]
    _1011,
    #[doc = "Circular buffer size is 32 KB."]
    _1100,
    #[doc = "Circular buffer size is 64 KB."]
    _1101,
    #[doc = "Circular buffer size is 128 KB."]
    _1110,
    #[doc = "Circular buffer size is 256 KB."]
    _1111,
}
impl crate::ToBits<u8> for SMOD_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            SMOD_A::_0000 => 0,
            SMOD_A::_0001 => 1,
            SMOD_A::_0010 => 2,
            SMOD_A::_0011 => 3,
            SMOD_A::_0100 => 4,
            SMOD_A::_0101 => 5,
            SMOD_A::_0110 => 6,
            SMOD_A::_0111 => 7,
            SMOD_A::_1000 => 8,
            SMOD_A::_1001 => 9,
            SMOD_A::_1010 => 10,
            SMOD_A::_1011 => 11,
            SMOD_A::_1100 => 12,
            SMOD_A::_1101 => 13,
            SMOD_A::_1110 => 14,
            SMOD_A::_1111 => 15,
        }
    }
}
#[doc = "Reader of field `SMOD`"]
pub type SMOD_R = crate::R<u8, SMOD_A>;
impl SMOD_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SMOD_A {
        match self.bits {
            0 => SMOD_A::_0000,
            1 => SMOD_A::_0001,
            2 => SMOD_A::_0010,
            3 => SMOD_A::_0011,
            4 => SMOD_A::_0100,
            5 => SMOD_A::_0101,
            6 => SMOD_A::_0110,
            7 => SMOD_A::_0111,
            8 => SMOD_A::_1000,
            9 => SMOD_A::_1001,
            10 => SMOD_A::_1010,
            11 => SMOD_A::_1011,
            12 => SMOD_A::_1100,
            13 => SMOD_A::_1101,
            14 => SMOD_A::_1110,
            15 => SMOD_A::_1111,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_0000`"]
    #[inline(always)]
    pub fn is_0000(&self) -> bool {
        *self == SMOD_A::_0000
    }
    #[doc = "Checks if the value of the field is `_0001`"]
    #[inline(always)]
    pub fn is_0001(&self) -> bool {
        *self == SMOD_A::_0001
    }
    #[doc = "Checks if the value of the field is `_0010`"]
    #[inline(always)]
    pub fn is_0010(&self) -> bool {
        *self == SMOD_A::_0010
    }
    #[doc = "Checks if the value of the field is `_0011`"]
    #[inline(always)]
    pub fn is_0011(&self) -> bool {
        *self == SMOD_A::_0011
    }
    #[doc = "Checks if the value of the field is `_0100`"]
    #[inline(always)]
    pub fn is_0100(&self) -> bool {
        *self == SMOD_A::_0100
    }
    #[doc = "Checks if the value of the field is `_0101`"]
    #[inline(always)]
    pub fn is_0101(&self) -> bool {
        *self == SMOD_A::_0101
    }
    #[doc = "Checks if the value of the field is `_0110`"]
    #[inline(always)]
    pub fn is_0110(&self) -> bool {
        *self == SMOD_A::_0110
    }
    #[doc = "Checks if the value of the field is `_0111`"]
    #[inline(always)]
    pub fn is_0111(&self) -> bool {
        *self == SMOD_A::_0111
    }
    #[doc = "Checks if the value of the field is `_1000`"]
    #[inline(always)]
    pub fn is_1000(&self) -> bool {
        *self == SMOD_A::_1000
    }
    #[doc = "Checks if the value of the field is `_1001`"]
    #[inline(always)]
    pub fn is_1001(&self) -> bool {
        *self == SMOD_A::_1001
    }
    #[doc = "Checks if the value of the field is `_1010`"]
    #[inline(always)]
    pub fn is_1010(&self) -> bool {
        *self == SMOD_A::_1010
    }
    #[doc = "Checks if the value of the field is `_1011`"]
    #[inline(always)]
    pub fn is_1011(&self) -> bool {
        *self == SMOD_A::_1011
    }
    #[doc = "Checks if the value of the field is `_1100`"]
    #[inline(always)]
    pub fn is_1100(&self) -> bool {
        *self == SMOD_A::_1100
    }
    #[doc = "Checks if the value of the field is `_1101`"]
    #[inline(always)]
    pub fn is_1101(&self) -> bool {
        *self == SMOD_A::_1101
    }
    #[doc = "Checks if the value of the field is `_1110`"]
    #[inline(always)]
    pub fn is_1110(&self) -> bool {
        *self == SMOD_A::_1110
    }
    #[doc = "Checks if the value of the field is `_1111`"]
    #[inline(always)]
    pub fn is_1111(&self) -> bool {
        *self == SMOD_A::_1111
    }
}
#[doc = "Write proxy for field `SMOD`"]
pub struct SMOD_W<'a> {
    w: &'a mut W,
}
impl<'a> SMOD_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: SMOD_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bits(variant._bits())
        }
    }
    #[doc = "Buffer disabled"]
    #[inline(always)]
    pub fn _0000(self) -> &'a mut W {
        self.variant(SMOD_A::_0000)
    }
    #[doc = "Circular buffer size is 16 bytes."]
    #[inline(always)]
    pub fn _0001(self) -> &'a mut W {
        self.variant(SMOD_A::_0001)
    }
    #[doc = "Circular buffer size is 32 bytes."]
    #[inline(always)]
    pub fn _0010(self) -> &'a mut W {
        self.variant(SMOD_A::_0010)
    }
    #[doc = "Circular buffer size is 64 bytes."]
    #[inline(always)]
    pub fn _0011(self) -> &'a mut W {
        self.variant(SMOD_A::_0011)
    }
    #[doc = "Circular buffer size is 128 bytes."]
    #[inline(always)]
    pub fn _0100(self) -> &'a mut W {
        self.variant(SMOD_A::_0100)
    }
    #[doc = "Circular buffer size is 256 bytes."]
    #[inline(always)]
    pub fn _0101(self) -> &'a mut W {
        self.variant(SMOD_A::_0101)
    }
    #[doc = "Circular buffer size is 512 bytes."]
    #[inline(always)]
    pub fn _0110(self) -> &'a mut W {
        self.variant(SMOD_A::_0110)
    }
    #[doc = "Circular buffer size is 1 KB."]
    #[inline(always)]
    pub fn _0111(self) -> &'a mut W {
        self.variant(SMOD_A::_0111)
    }
    #[doc = "Circular buffer size is 2 KB."]
    #[inline(always)]
    pub fn _1000(self) -> &'a mut W {
        self.variant(SMOD_A::_1000)
    }
    #[doc = "Circular buffer size is 4 KB."]
    #[inline(always)]
    pub fn _1001(self) -> &'a mut W {
        self.variant(SMOD_A::_1001)
    }
    #[doc = "Circular buffer size is 8 KB."]
    #[inline(always)]
    pub fn _1010(self) -> &'a mut W {
        self.variant(SMOD_A::_1010)
    }
    #[doc = "Circular buffer size is 16 KB."]
    #[inline(always)]
    pub fn _1011(self) -> &'a mut W {
        self.variant(SMOD_A::_1011)
    }
    #[doc = "Circular buffer size is 32 KB."]
    #[inline(always)]
    pub fn _1100(self) -> &'a mut W {
        self.variant(SMOD_A::_1100)
    }
    #[doc = "Circular buffer size is 64 KB."]
    #[inline(always)]
    pub fn _1101(self) -> &'a mut W {
        self.variant(SMOD_A::_1101)
    }
    #[doc = "Circular buffer size is 128 KB."]
    #[inline(always)]
    pub fn _1110(self) -> &'a mut W {
        self.variant(SMOD_A::_1110)
    }
    #[doc = "Circular buffer size is 256 KB."]
    #[inline(always)]
    pub fn _1111(self) -> &'a mut W {
        self.variant(SMOD_A::_1111)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bits(self, value: u8) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x0f << 12)) | (((value as u32) & 0x0f) << 12);
        self.w
    }
}
#[doc = "Possible values of the field `START`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum START_AW {
    #[doc = "DMA inactive"]
    _0,
    #[doc = "The DMA begins the transfer in accordance to the values in the TCDn. START is cleared automatically after one module clock and always reads as logic 0."]
    _1,
}
impl crate::ToBits<bool> for START_AW {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            START_AW::_0 => false,
            START_AW::_1 => true,
        }
    }
}
#[doc = "Write proxy for field `START`"]
pub struct START_W<'a> {
    w: &'a mut W,
}
impl<'a> START_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: START_AW) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "DMA inactive"]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(START_AW::_0)
    }
    #[doc = "The DMA begins the transfer in accordance to the values in the TCDn. START is cleared automatically after one module clock and always reads as logic 0."]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(START_AW::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 16)) | (((value as u32) & 0x01) << 16);
        self.w
    }
}
#[doc = "Possible values of the field `DSIZE`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum DSIZE_A {
    #[doc = "32-bit"]
    _00,
    #[doc = "8-bit"]
    _01,
    #[doc = "16-bit"]
    _10,
    #[doc = "Reserved (generates a configuration error (DSRn\\[CE\\]) if incorrectly specified at time of channel activation)"]
    _11,
}
impl crate::ToBits<u8> for DSIZE_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            DSIZE_A::_00 => 0,
            DSIZE_A::_01 => 1,
            DSIZE_A::_10 => 2,
            DSIZE_A::_11 => 3,
        }
    }
}
#[doc = "Reader of field `DSIZE`"]
pub type DSIZE_R = crate::R<u8, DSIZE_A>;
impl DSIZE_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DSIZE_A {
        match self.bits {
            0 => DSIZE_A::_00,
            1 => DSIZE_A::_01,
            2 => DSIZE_A::_10,
            3 => DSIZE_A::_11,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_00`"]
    #[inline(always)]
    pub fn is_00(&self) -> bool {
        *self == DSIZE_A::_00
    }
    #[doc = "Checks if the value of the field is `_01`"]
    #[inline(always)]
    pub fn is_01(&self) -> bool {
        *self == DSIZE_A::_01
    }
    #[doc = "Checks if the value of the field is `_10`"]
    #[inline(always)]
    pub fn is_10(&self) -> bool {
        *self == DSIZE_A::_10
    }
    #[doc = "Checks if the value of the field is `_11`"]
    #[inline(always)]
    pub fn is_11(&self) -> bool {
        *self == DSIZE_A::_11
    }
}
#[doc = "Write proxy for field `DSIZE`"]
pub struct DSIZE_W<'a> {
    w: &'a mut W,
}
impl<'a> DSIZE_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: DSIZE_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bits(variant._bits())
        }
    }
    #[doc = "32-bit"]
    #[inline(always)]
    pub fn _00(self) -> &'a mut W {
        self.variant(DSIZE_A::_00)
    }
    #[doc = "8-bit"]
    #[inline(always)]
    pub fn _01(self) -> &'a mut W {
        self.variant(DSIZE_A::_01)
    }
    #[doc = "16-bit"]
    #[inline(always)]
    pub fn _10(self) -> &'a mut W {
        self.variant(DSIZE_A::_10)
    }
    #[doc = "Reserved (generates a configuration error (DSRn\\[CE\\]) if incorrectly specified at time of channel activation)"]
    #[inline(always)]
    pub fn _11(self) -> &'a mut W {
        self.variant(DSIZE_A::_11)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bits(self, value: u8) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x03 << 17)) | (((value as u32) & 0x03) << 17);
        self.w
    }
}
#[doc = "Possible values of the field `DINC`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum DINC_A {
    #[doc = "No change to the DAR after a successful transfer."]
    _0,
    #[doc = "The DAR increments by 1, 2, 4 depending upon the size of the transfer."]
    _1,
}
impl crate::ToBits<bool> for DINC_A {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            DINC_A::_0 => false,
            DINC_A::_1 => true,
        }
    }
}
#[doc = "Reader of field `DINC`"]
pub type DINC_R = crate::R<bool, DINC_A>;
impl DINC_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DINC_A {
        match self.bits {
            false => DINC_A::_0,
            true => DINC_A::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == DINC_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == DINC_A::_1
    }
}
#[doc = "Write proxy for field `DINC`"]
pub struct DINC_W<'a> {
    w: &'a mut W,
}
impl<'a> DINC_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: DINC_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "No change to the DAR after a successful transfer."]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(DINC_A::_0)
    }
    #[doc = "The DAR increments by 1, 2, 4 depending upon the size of the transfer."]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(DINC_A::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 19)) | (((value as u32) & 0x01) << 19);
        self.w
    }
}
#[doc = "Possible values of the field `SSIZE`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum SSIZE_A {
    #[doc = "32-bit"]
    _00,
    #[doc = "8-bit"]
    _01,
    #[doc = "16-bit"]
    _10,
    #[doc = "Reserved (generates a configuration error (DSRn\\[CE\\]) if incorrectly specified at time of channel activation)"]
    _11,
}
impl crate::ToBits<u8> for SSIZE_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            SSIZE_A::_00 => 0,
            SSIZE_A::_01 => 1,
            SSIZE_A::_10 => 2,
            SSIZE_A::_11 => 3,
        }
    }
}
#[doc = "Reader of field `SSIZE`"]
pub type SSIZE_R = crate::R<u8, SSIZE_A>;
impl SSIZE_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SSIZE_A {
        match self.bits {
            0 => SSIZE_A::_00,
            1 => SSIZE_A::_01,
            2 => SSIZE_A::_10,
            3 => SSIZE_A::_11,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_00`"]
    #[inline(always)]
    pub fn is_00(&self) -> bool {
        *self == SSIZE_A::_00
    }
    #[doc = "Checks if the value of the field is `_01`"]
    #[inline(always)]
    pub fn is_01(&self) -> bool {
        *self == SSIZE_A::_01
    }
    #[doc = "Checks if the value of the field is `_10`"]
    #[inline(always)]
    pub fn is_10(&self) -> bool {
        *self == SSIZE_A::_10
    }
    #[doc = "Checks if the value of the field is `_11`"]
    #[inline(always)]
    pub fn is_11(&self) -> bool {
        *self == SSIZE_A::_11
    }
}
#[doc = "Write proxy for field `SSIZE`"]
pub struct SSIZE_W<'a> {
    w: &'a mut W,
}
impl<'a> SSIZE_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: SSIZE_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bits(variant._bits())
        }
    }
    #[doc = "32-bit"]
    #[inline(always)]
    pub fn _00(self) -> &'a mut W {
        self.variant(SSIZE_A::_00)
    }
    #[doc = "8-bit"]
    #[inline(always)]
    pub fn _01(self) -> &'a mut W {
        self.variant(SSIZE_A::_01)
    }
    #[doc = "16-bit"]
    #[inline(always)]
    pub fn _10(self) -> &'a mut W {
        self.variant(SSIZE_A::_10)
    }
    #[doc = "Reserved (generates a configuration error (DSRn\\[CE\\]) if incorrectly specified at time of channel activation)"]
    #[inline(always)]
    pub fn _11(self) -> &'a mut W {
        self.variant(SSIZE_A::_11)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bits(self, value: u8) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x03 << 20)) | (((value as u32) & 0x03) << 20);
        self.w
    }
}
#[doc = "Possible values of the field `SINC`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum SINC_A {
    #[doc = "No change to SAR after a successful transfer."]
    _0,
    #[doc = "The SAR increments by 1, 2, 4 as determined by the transfer size."]
    _1,
}
impl crate::ToBits<bool> for SINC_A {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            SINC_A::_0 => false,
            SINC_A::_1 => true,
        }
    }
}
#[doc = "Reader of field `SINC`"]
pub type SINC_R = crate::R<bool, SINC_A>;
impl SINC_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SINC_A {
        match self.bits {
            false => SINC_A::_0,
            true => SINC_A::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == SINC_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == SINC_A::_1
    }
}
#[doc = "Write proxy for field `SINC`"]
pub struct SINC_W<'a> {
    w: &'a mut W,
}
impl<'a> SINC_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: SINC_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "No change to SAR after a successful transfer."]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(SINC_A::_0)
    }
    #[doc = "The SAR increments by 1, 2, 4 as determined by the transfer size."]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(SINC_A::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 22)) | (((value as u32) & 0x01) << 22);
        self.w
    }
}
#[doc = "Possible values of the field `EADREQ`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum EADREQ_A {
    #[doc = "Disabled"]
    _0,
    #[doc = "Enabled"]
    _1,
}
impl crate::ToBits<bool> for EADREQ_A {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            EADREQ_A::_0 => false,
            EADREQ_A::_1 => true,
        }
    }
}
#[doc = "Reader of field `EADREQ`"]
pub type EADREQ_R = crate::R<bool, EADREQ_A>;
impl EADREQ_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> EADREQ_A {
        match self.bits {
            false => EADREQ_A::_0,
            true => EADREQ_A::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == EADREQ_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == EADREQ_A::_1
    }
}
#[doc = "Write proxy for field `EADREQ`"]
pub struct EADREQ_W<'a> {
    w: &'a mut W,
}
impl<'a> EADREQ_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: EADREQ_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "Disabled"]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(EADREQ_A::_0)
    }
    #[doc = "Enabled"]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(EADREQ_A::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 23)) | (((value as u32) & 0x01) << 23);
        self.w
    }
}
#[doc = "Possible values of the field `AA`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum AA_A {
    #[doc = "Auto-align disabled"]
    _0,
    #[doc = "If SSIZE indicates a transfer no smaller than DSIZE, source accesses are auto-aligned; otherwise, destination accesses are auto-aligned. Source alignment takes precedence over destination alignment. If auto-alignment is enabled, the appropriate address register increments, regardless of DINC or SINC."]
    _1,
}
impl crate::ToBits<bool> for AA_A {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            AA_A::_0 => false,
            AA_A::_1 => true,
        }
    }
}
#[doc = "Reader of field `AA`"]
pub type AA_R = crate::R<bool, AA_A>;
impl AA_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> AA_A {
        match self.bits {
            false => AA_A::_0,
            true => AA_A::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == AA_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == AA_A::_1
    }
}
#[doc = "Write proxy for field `AA`"]
pub struct AA_W<'a> {
    w: &'a mut W,
}
impl<'a> AA_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: AA_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "Auto-align disabled"]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(AA_A::_0)
    }
    #[doc = "If SSIZE indicates a transfer no smaller than DSIZE, source accesses are auto-aligned; otherwise, destination accesses are auto-aligned. Source alignment takes precedence over destination alignment. If auto-alignment is enabled, the appropriate address register increments, regardless of DINC or SINC."]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(AA_A::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 28)) | (((value as u32) & 0x01) << 28);
        self.w
    }
}
#[doc = "Possible values of the field `CS`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum CS_A {
    #[doc = "DMA continuously makes read/write transfers until the BCR decrements to 0."]
    _0,
    #[doc = "Forces a single read/write transfer per request."]
    _1,
}
impl crate::ToBits<bool> for CS_A {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            CS_A::_0 => false,
            CS_A::_1 => true,
        }
    }
}
#[doc = "Reader of field `CS`"]
pub type CS_R = crate::R<bool, CS_A>;
impl CS_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CS_A {
        match self.bits {
            false => CS_A::_0,
            true => CS_A::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == CS_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == CS_A::_1
    }
}
#[doc = "Write proxy for field `CS`"]
pub struct CS_W<'a> {
    w: &'a mut W,
}
impl<'a> CS_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: CS_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "DMA continuously makes read/write transfers until the BCR decrements to 0."]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(CS_A::_0)
    }
    #[doc = "Forces a single read/write transfer per request."]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(CS_A::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 29)) | (((value as u32) & 0x01) << 29);
        self.w
    }
}
#[doc = "Possible values of the field `ERQ`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum ERQ_A {
    #[doc = "Peripheral request is ignored."]
    _0,
    #[doc = "Enables peripheral request to initiate transfer. A software-initiated request (setting START) is always enabled."]
    _1,
}
impl crate::ToBits<bool> for ERQ_A {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            ERQ_A::_0 => false,
            ERQ_A::_1 => true,
        }
    }
}
#[doc = "Reader of field `ERQ`"]
pub type ERQ_R = crate::R<bool, ERQ_A>;
impl ERQ_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> ERQ_A {
        match self.bits {
            false => ERQ_A::_0,
            true => ERQ_A::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == ERQ_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == ERQ_A::_1
    }
}
#[doc = "Write proxy for field `ERQ`"]
pub struct ERQ_W<'a> {
    w: &'a mut W,
}
impl<'a> ERQ_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: ERQ_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "Peripheral request is ignored."]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(ERQ_A::_0)
    }
    #[doc = "Enables peripheral request to initiate transfer. A software-initiated request (setting START) is always enabled."]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(ERQ_A::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 30)) | (((value as u32) & 0x01) << 30);
        self.w
    }
}
#[doc = "Possible values of the field `EINT`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum EINT_A {
    #[doc = "No interrupt is generated."]
    _0,
    #[doc = "Interrupt signal is enabled."]
    _1,
}
impl crate::ToBits<bool> for EINT_A {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            EINT_A::_0 => false,
            EINT_A::_1 => true,
        }
    }
}
#[doc = "Reader of field `EINT`"]
pub type EINT_R = crate::R<bool, EINT_A>;
impl EINT_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> EINT_A {
        match self.bits {
            false => EINT_A::_0,
            true => EINT_A::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == EINT_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == EINT_A::_1
    }
}
#[doc = "Write proxy for field `EINT`"]
pub struct EINT_W<'a> {
    w: &'a mut W,
}
impl<'a> EINT_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: EINT_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "No interrupt is generated."]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(EINT_A::_0)
    }
    #[doc = "Interrupt signal is enabled."]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(EINT_A::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 31)) | (((value as u32) & 0x01) << 31);
        self.w
    }
}
impl R {
    #[doc = "Bits 0:1 - Link Channel 2"]
    #[inline(always)]
    pub fn lch2(&self) -> LCH2_R {
        LCH2_R::new((self.bits & 0x03) as u8)
    }
    #[doc = "Bits 2:3 - Link Channel 1"]
    #[inline(always)]
    pub fn lch1(&self) -> LCH1_R {
        LCH1_R::new(((self.bits >> 2) & 0x03) as u8)
    }
    #[doc = "Bits 4:5 - Link Channel Control"]
    #[inline(always)]
    pub fn linkcc(&self) -> LINKCC_R {
        LINKCC_R::new(((self.bits >> 4) & 0x03) as u8)
    }
    #[doc = "Bit 7 - Disable Request"]
    #[inline(always)]
    pub fn d_req(&self) -> D_REQ_R {
        D_REQ_R::new(((self.bits >> 7) & 0x01) != 0)
    }
    #[doc = "Bits 8:11 - Destination Address Modulo"]
    #[inline(always)]
    pub fn dmod(&self) -> DMOD_R {
        DMOD_R::new(((self.bits >> 8) & 0x0f) as u8)
    }
    #[doc = "Bits 12:15 - Source Address Modulo"]
    #[inline(always)]
    pub fn smod(&self) -> SMOD_R {
        SMOD_R::new(((self.bits >> 12) & 0x0f) as u8)
    }
    #[doc = "Bits 17:18 - Destination Size"]
    #[inline(always)]
    pub fn dsize(&self) -> DSIZE_R {
        DSIZE_R::new(((self.bits >> 17) & 0x03) as u8)
    }
    #[doc = "Bit 19 - Destination Increment"]
    #[inline(always)]
    pub fn dinc(&self) -> DINC_R {
        DINC_R::new(((self.bits >> 19) & 0x01) != 0)
    }
    #[doc = "Bits 20:21 - Source Size"]
    #[inline(always)]
    pub fn ssize(&self) -> SSIZE_R {
        SSIZE_R::new(((self.bits >> 20) & 0x03) as u8)
    }
    #[doc = "Bit 22 - Source Increment"]
    #[inline(always)]
    pub fn sinc(&self) -> SINC_R {
        SINC_R::new(((self.bits >> 22) & 0x01) != 0)
    }
    #[doc = "Bit 23 - Enable asynchronous DMA requests"]
    #[inline(always)]
    pub fn eadreq(&self) -> EADREQ_R {
        EADREQ_R::new(((self.bits >> 23) & 0x01) != 0)
    }
    #[doc = "Bit 28 - Auto-align"]
    #[inline(always)]
    pub fn aa(&self) -> AA_R {
        AA_R::new(((self.bits >> 28) & 0x01) != 0)
    }
    #[doc = "Bit 29 - Cycle Steal"]
    #[inline(always)]
    pub fn cs(&self) -> CS_R {
        CS_R::new(((self.bits >> 29) & 0x01) != 0)
    }
    #[doc = "Bit 30 - Enable Peripheral Request"]
    #[inline(always)]
    pub fn erq(&self) -> ERQ_R {
        ERQ_R::new(((self.bits >> 30) & 0x01) != 0)
    }
    #[doc = "Bit 31 - Enable Interrupt on Completion of Transfer"]
    #[inline(always)]
    pub fn eint(&self) -> EINT_R {
        EINT_R::new(((self.bits >> 31) & 0x01) != 0)
    }
}
impl W {
    #[doc = "Bits 0:1 - Link Channel 2"]
    #[inline(always)]
    pub fn lch2(&mut self) -> LCH2_W {
        LCH2_W { w: self }
    }
    #[doc = "Bits 2:3 - Link Channel 1"]
    #[inline(always)]
    pub fn lch1(&mut self) -> LCH1_W {
        LCH1_W { w: self }
    }
    #[doc = "Bits 4:5 - Link Channel Control"]
    #[inline(always)]
    pub fn linkcc(&mut self) -> LINKCC_W {
        LINKCC_W { w: self }
    }
    #[doc = "Bit 7 - Disable Request"]
    #[inline(always)]
    pub fn d_req(&mut self) -> D_REQ_W {
        D_REQ_W { w: self }
    }
    #[doc = "Bits 8:11 - Destination Address Modulo"]
    #[inline(always)]
    pub fn dmod(&mut self) -> DMOD_W {
        DMOD_W { w: self }
    }
    #[doc = "Bits 12:15 - Source Address Modulo"]
    #[inline(always)]
    pub fn smod(&mut self) -> SMOD_W {
        SMOD_W { w: self }
    }
    #[doc = "Bit 16 - Start Transfer"]
    #[inline(always)]
    pub fn start(&mut self) -> START_W {
        START_W { w: self }
    }
    #[doc = "Bits 17:18 - Destination Size"]
    #[inline(always)]
    pub fn dsize(&mut self) -> DSIZE_W {
        DSIZE_W { w: self }
    }
    #[doc = "Bit 19 - Destination Increment"]
    #[inline(always)]
    pub fn dinc(&mut self) -> DINC_W {
        DINC_W { w: self }
    }
    #[doc = "Bits 20:21 - Source Size"]
    #[inline(always)]
    pub fn ssize(&mut self) -> SSIZE_W {
        SSIZE_W { w: self }
    }
    #[doc = "Bit 22 - Source Increment"]
    #[inline(always)]
    pub fn sinc(&mut self) -> SINC_W {
        SINC_W { w: self }
    }
    #[doc = "Bit 23 - Enable asynchronous DMA requests"]
    #[inline(always)]
    pub fn eadreq(&mut self) -> EADREQ_W {
        EADREQ_W { w: self }
    }
    #[doc = "Bit 28 - Auto-align"]
    #[inline(always)]
    pub fn aa(&mut self) -> AA_W {
        AA_W { w: self }
    }
    #[doc = "Bit 29 - Cycle Steal"]
    #[inline(always)]
    pub fn cs(&mut self) -> CS_W {
        CS_W { w: self }
    }
    #[doc = "Bit 30 - Enable Peripheral Request"]
    #[inline(always)]
    pub fn erq(&mut self) -> ERQ_W {
        ERQ_W { w: self }
    }
    #[doc = "Bit 31 - Enable Interrupt on Completion of Transfer"]
    #[inline(always)]
    pub fn eint(&mut self) -> EINT_W {
        EINT_W { w: self }
    }
}
