#[doc = "Reader of register SDID"]
pub type R = crate::R<u32, super::SDID>;
#[doc = "Possible values of the field `PINID`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PINID_A {
    #[doc = "16-pin"]
    _0000,
    #[doc = "24-pin"]
    _0001,
    #[doc = "32-pin"]
    _0010,
    #[doc = "36-pin"]
    _0011,
    #[doc = "48-pin"]
    _0100,
    #[doc = "64-pin"]
    _0101,
    #[doc = "80-pin"]
    _0110,
    #[doc = "100-pin"]
    _1000,
    #[doc = "Custom pinout (WLCSP)"]
    _1011,
}
impl crate::ToBits<u8> for PINID_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            PINID_A::_0000 => 0,
            PINID_A::_0001 => 1,
            PINID_A::_0010 => 2,
            PINID_A::_0011 => 3,
            PINID_A::_0100 => 4,
            PINID_A::_0101 => 5,
            PINID_A::_0110 => 6,
            PINID_A::_1000 => 8,
            PINID_A::_1011 => 11,
        }
    }
}
#[doc = "Reader of field `PINID`"]
pub type PINID_R = crate::R<u8, PINID_A>;
impl PINID_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> crate::Variant<u8, PINID_A> {
        use crate::Variant::*;
        match self.bits {
            0 => Val(PINID_A::_0000),
            1 => Val(PINID_A::_0001),
            2 => Val(PINID_A::_0010),
            3 => Val(PINID_A::_0011),
            4 => Val(PINID_A::_0100),
            5 => Val(PINID_A::_0101),
            6 => Val(PINID_A::_0110),
            8 => Val(PINID_A::_1000),
            11 => Val(PINID_A::_1011),
            i => Res(i),
        }
    }
    #[doc = "Checks if the value of the field is `_0000`"]
    #[inline(always)]
    pub fn is_0000(&self) -> bool {
        *self == PINID_A::_0000
    }
    #[doc = "Checks if the value of the field is `_0001`"]
    #[inline(always)]
    pub fn is_0001(&self) -> bool {
        *self == PINID_A::_0001
    }
    #[doc = "Checks if the value of the field is `_0010`"]
    #[inline(always)]
    pub fn is_0010(&self) -> bool {
        *self == PINID_A::_0010
    }
    #[doc = "Checks if the value of the field is `_0011`"]
    #[inline(always)]
    pub fn is_0011(&self) -> bool {
        *self == PINID_A::_0011
    }
    #[doc = "Checks if the value of the field is `_0100`"]
    #[inline(always)]
    pub fn is_0100(&self) -> bool {
        *self == PINID_A::_0100
    }
    #[doc = "Checks if the value of the field is `_0101`"]
    #[inline(always)]
    pub fn is_0101(&self) -> bool {
        *self == PINID_A::_0101
    }
    #[doc = "Checks if the value of the field is `_0110`"]
    #[inline(always)]
    pub fn is_0110(&self) -> bool {
        *self == PINID_A::_0110
    }
    #[doc = "Checks if the value of the field is `_1000`"]
    #[inline(always)]
    pub fn is_1000(&self) -> bool {
        *self == PINID_A::_1000
    }
    #[doc = "Checks if the value of the field is `_1011`"]
    #[inline(always)]
    pub fn is_1011(&self) -> bool {
        *self == PINID_A::_1011
    }
}
#[doc = "Reader of field `DIEID`"]
pub type DIEID_R = crate::R<u8, u8>;
#[doc = "Reader of field `REVID`"]
pub type REVID_R = crate::R<u8, u8>;
#[doc = "Possible values of the field `SRAMSIZE`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum SRAMSIZE_A {
    #[doc = "0.5 KB"]
    _0000,
    #[doc = "1 KB"]
    _0001,
    #[doc = "2 KB"]
    _0010,
    #[doc = "4 KB"]
    _0011,
    #[doc = "8 KB"]
    _0100,
    #[doc = "16 KB"]
    _0101,
    #[doc = "32 KB"]
    _0110,
    #[doc = "64 KB"]
    _0111,
}
impl crate::ToBits<u8> for SRAMSIZE_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            SRAMSIZE_A::_0000 => 0,
            SRAMSIZE_A::_0001 => 1,
            SRAMSIZE_A::_0010 => 2,
            SRAMSIZE_A::_0011 => 3,
            SRAMSIZE_A::_0100 => 4,
            SRAMSIZE_A::_0101 => 5,
            SRAMSIZE_A::_0110 => 6,
            SRAMSIZE_A::_0111 => 7,
        }
    }
}
#[doc = "Reader of field `SRAMSIZE`"]
pub type SRAMSIZE_R = crate::R<u8, SRAMSIZE_A>;
impl SRAMSIZE_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> crate::Variant<u8, SRAMSIZE_A> {
        use crate::Variant::*;
        match self.bits {
            0 => Val(SRAMSIZE_A::_0000),
            1 => Val(SRAMSIZE_A::_0001),
            2 => Val(SRAMSIZE_A::_0010),
            3 => Val(SRAMSIZE_A::_0011),
            4 => Val(SRAMSIZE_A::_0100),
            5 => Val(SRAMSIZE_A::_0101),
            6 => Val(SRAMSIZE_A::_0110),
            7 => Val(SRAMSIZE_A::_0111),
            i => Res(i),
        }
    }
    #[doc = "Checks if the value of the field is `_0000`"]
    #[inline(always)]
    pub fn is_0000(&self) -> bool {
        *self == SRAMSIZE_A::_0000
    }
    #[doc = "Checks if the value of the field is `_0001`"]
    #[inline(always)]
    pub fn is_0001(&self) -> bool {
        *self == SRAMSIZE_A::_0001
    }
    #[doc = "Checks if the value of the field is `_0010`"]
    #[inline(always)]
    pub fn is_0010(&self) -> bool {
        *self == SRAMSIZE_A::_0010
    }
    #[doc = "Checks if the value of the field is `_0011`"]
    #[inline(always)]
    pub fn is_0011(&self) -> bool {
        *self == SRAMSIZE_A::_0011
    }
    #[doc = "Checks if the value of the field is `_0100`"]
    #[inline(always)]
    pub fn is_0100(&self) -> bool {
        *self == SRAMSIZE_A::_0100
    }
    #[doc = "Checks if the value of the field is `_0101`"]
    #[inline(always)]
    pub fn is_0101(&self) -> bool {
        *self == SRAMSIZE_A::_0101
    }
    #[doc = "Checks if the value of the field is `_0110`"]
    #[inline(always)]
    pub fn is_0110(&self) -> bool {
        *self == SRAMSIZE_A::_0110
    }
    #[doc = "Checks if the value of the field is `_0111`"]
    #[inline(always)]
    pub fn is_0111(&self) -> bool {
        *self == SRAMSIZE_A::_0111
    }
}
#[doc = "Possible values of the field `SERIESID`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum SERIESID_A {
    #[doc = "KL family"]
    _0001,
}
impl crate::ToBits<u8> for SERIESID_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            SERIESID_A::_0001 => 1,
        }
    }
}
#[doc = "Reader of field `SERIESID`"]
pub type SERIESID_R = crate::R<u8, SERIESID_A>;
impl SERIESID_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> crate::Variant<u8, SERIESID_A> {
        use crate::Variant::*;
        match self.bits {
            1 => Val(SERIESID_A::_0001),
            i => Res(i),
        }
    }
    #[doc = "Checks if the value of the field is `_0001`"]
    #[inline(always)]
    pub fn is_0001(&self) -> bool {
        *self == SERIESID_A::_0001
    }
}
#[doc = "Possible values of the field `SUBFAMID`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum SUBFAMID_A {
    #[doc = "KLx2 Subfamily"]
    _0010,
    #[doc = "KLx3 Subfamily"]
    _0011,
    #[doc = "KLx4 Subfamily"]
    _0100,
    #[doc = "KLx5 Subfamily"]
    _0101,
    #[doc = "KLx6 Subfamily"]
    _0110,
    #[doc = "KLx7 Subfamily"]
    _0111,
}
impl crate::ToBits<u8> for SUBFAMID_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            SUBFAMID_A::_0010 => 2,
            SUBFAMID_A::_0011 => 3,
            SUBFAMID_A::_0100 => 4,
            SUBFAMID_A::_0101 => 5,
            SUBFAMID_A::_0110 => 6,
            SUBFAMID_A::_0111 => 7,
        }
    }
}
#[doc = "Reader of field `SUBFAMID`"]
pub type SUBFAMID_R = crate::R<u8, SUBFAMID_A>;
impl SUBFAMID_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> crate::Variant<u8, SUBFAMID_A> {
        use crate::Variant::*;
        match self.bits {
            2 => Val(SUBFAMID_A::_0010),
            3 => Val(SUBFAMID_A::_0011),
            4 => Val(SUBFAMID_A::_0100),
            5 => Val(SUBFAMID_A::_0101),
            6 => Val(SUBFAMID_A::_0110),
            7 => Val(SUBFAMID_A::_0111),
            i => Res(i),
        }
    }
    #[doc = "Checks if the value of the field is `_0010`"]
    #[inline(always)]
    pub fn is_0010(&self) -> bool {
        *self == SUBFAMID_A::_0010
    }
    #[doc = "Checks if the value of the field is `_0011`"]
    #[inline(always)]
    pub fn is_0011(&self) -> bool {
        *self == SUBFAMID_A::_0011
    }
    #[doc = "Checks if the value of the field is `_0100`"]
    #[inline(always)]
    pub fn is_0100(&self) -> bool {
        *self == SUBFAMID_A::_0100
    }
    #[doc = "Checks if the value of the field is `_0101`"]
    #[inline(always)]
    pub fn is_0101(&self) -> bool {
        *self == SUBFAMID_A::_0101
    }
    #[doc = "Checks if the value of the field is `_0110`"]
    #[inline(always)]
    pub fn is_0110(&self) -> bool {
        *self == SUBFAMID_A::_0110
    }
    #[doc = "Checks if the value of the field is `_0111`"]
    #[inline(always)]
    pub fn is_0111(&self) -> bool {
        *self == SUBFAMID_A::_0111
    }
}
#[doc = "Possible values of the field `FAMID`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum FAMID_A {
    #[doc = "KL0x Family (low end)"]
    _0000,
    #[doc = "KL1x Family (basic)"]
    _0001,
    #[doc = "KL2x Family (USB)"]
    _0010,
    #[doc = "KL3x Family (Segment LCD)"]
    _0011,
    #[doc = "KL4x Family (USB and Segment LCD)"]
    _0100,
}
impl crate::ToBits<u8> for FAMID_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            FAMID_A::_0000 => 0,
            FAMID_A::_0001 => 1,
            FAMID_A::_0010 => 2,
            FAMID_A::_0011 => 3,
            FAMID_A::_0100 => 4,
        }
    }
}
#[doc = "Reader of field `FAMID`"]
pub type FAMID_R = crate::R<u8, FAMID_A>;
impl FAMID_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> crate::Variant<u8, FAMID_A> {
        use crate::Variant::*;
        match self.bits {
            0 => Val(FAMID_A::_0000),
            1 => Val(FAMID_A::_0001),
            2 => Val(FAMID_A::_0010),
            3 => Val(FAMID_A::_0011),
            4 => Val(FAMID_A::_0100),
            i => Res(i),
        }
    }
    #[doc = "Checks if the value of the field is `_0000`"]
    #[inline(always)]
    pub fn is_0000(&self) -> bool {
        *self == FAMID_A::_0000
    }
    #[doc = "Checks if the value of the field is `_0001`"]
    #[inline(always)]
    pub fn is_0001(&self) -> bool {
        *self == FAMID_A::_0001
    }
    #[doc = "Checks if the value of the field is `_0010`"]
    #[inline(always)]
    pub fn is_0010(&self) -> bool {
        *self == FAMID_A::_0010
    }
    #[doc = "Checks if the value of the field is `_0011`"]
    #[inline(always)]
    pub fn is_0011(&self) -> bool {
        *self == FAMID_A::_0011
    }
    #[doc = "Checks if the value of the field is `_0100`"]
    #[inline(always)]
    pub fn is_0100(&self) -> bool {
        *self == FAMID_A::_0100
    }
}
impl R {
    #[doc = "Bits 0:3 - Pincount Identification"]
    #[inline(always)]
    pub fn pinid(&self) -> PINID_R {
        PINID_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bits 7:11 - Device Die Number"]
    #[inline(always)]
    pub fn dieid(&self) -> DIEID_R {
        DIEID_R::new(((self.bits >> 7) & 0x1f) as u8)
    }
    #[doc = "Bits 12:15 - Device Revision Number"]
    #[inline(always)]
    pub fn revid(&self) -> REVID_R {
        REVID_R::new(((self.bits >> 12) & 0x0f) as u8)
    }
    #[doc = "Bits 16:19 - System SRAM Size"]
    #[inline(always)]
    pub fn sramsize(&self) -> SRAMSIZE_R {
        SRAMSIZE_R::new(((self.bits >> 16) & 0x0f) as u8)
    }
    #[doc = "Bits 20:23 - Kinetis Series ID"]
    #[inline(always)]
    pub fn seriesid(&self) -> SERIESID_R {
        SERIESID_R::new(((self.bits >> 20) & 0x0f) as u8)
    }
    #[doc = "Bits 24:27 - Kinetis Sub-Family ID"]
    #[inline(always)]
    pub fn subfamid(&self) -> SUBFAMID_R {
        SUBFAMID_R::new(((self.bits >> 24) & 0x0f) as u8)
    }
    #[doc = "Bits 28:31 - Kinetis family ID"]
    #[inline(always)]
    pub fn famid(&self) -> FAMID_R {
        FAMID_R::new(((self.bits >> 28) & 0x0f) as u8)
    }
}
