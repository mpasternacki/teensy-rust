#![no_std]
#![no_main]
#![feature(asm)]

extern crate teensy_lc;

use cortex_m;
use cortex_m_rt::{entry, pre_init};
use teensy_lc::{prelude::*, sim::Srvcop};
// use teensy_lc::sim::{Copc, COPTime};

#[pre_init]
unsafe fn init() {
    teensy_lc::init();
}

#[entry]
fn main() -> ! {
    let pp = teensy_lc::dev::Peripherals::take().unwrap();
    // let cpp = dev::CorePeripherals::take().unwrap();

    // Configure watchdog (this example works only with default LPO1024ms):
    // Copc(&pp.SIM.copc).start(COPTime::LPO256ms);

    // or disable it entirely:
    // Copc(&pp.SIM.copc).disable();
    let mut wd = Srvcop(&pp.SIM.srvcop);

    pp.MCG.constrain().init_clock_48mhz(&pp.SIM, &pp.OSC0);

    //// Initialize LED to on
    // Port C Clock Gate Control: enable clock to port C
    pp.SIM.scgc5.write(|w| w.portc().set_bit());

    let fgpioc = pp.FGPIOC.split();
    fgpioc.pc5.enable();
    let mut led_pin = fgpioc.pc5.into_output();

    for _ in 0..3 {
        wd.feed();
        led_pin.set_high().unwrap();
        teensy_lc::delay_usec(50_000);
        led_pin.set_low().unwrap();
        teensy_lc::delay_usec(50_000);
    }

    for _ in 0..4 {
        wd.feed();
        teensy_lc::delay_usec(200_000);
    }

    cortex_m::interrupt::disable();
    loop {
        wd.feed();
        led_pin.set_high().unwrap();
        teensy_lc::delay_usec(100_000);
        led_pin.set_low().unwrap();
        teensy_lc::delay_usec(900_000);
    }
}
