#![feature(never_type)]
#![no_std]

pub extern crate embedded_hal as hal;
pub extern crate mkl26z4 as dev;
extern crate teensy_lc_macros;

pub mod fgpio;
pub mod mcg;
pub mod panic_sos;
pub mod prelude;
pub mod sim;

// TODO: how to make clock selection configurable and still well-optimized?
pub const F_CPU: u32 = 48_000_000;
// F_BUS = 24000000;
// F_MEM = 24000000;
// F_PLL = 96000000;

pub fn delay_usec(usec: u32) {
    cortex_m::asm::delay(usec * (F_CPU / 1_000_000));
}

pub fn init() {
    // SKIP:
    // enable clocks to always used peripherals
    // In teensyduino it turns on USB OTG, GPIO, ADC0, TPM, flash
    // (last one seems enabled by default)

    // release I/O pins hold, if we woke up from VLLS mode
    // if (PMC_REGSC & PMC_REGSC_ACKISO) PMC_REGSC |= PMC_REGSC_ACKISO;
    let regsc = unsafe { &(*dev::PMC::ptr()).regsc };
    if regsc.read().ackiso().is_1() {
        regsc.modify(|_, w| w.ackiso().set_bit());
    }

    // since this is a write once register, make it visible to all F_CPU's
    // so we can into other sleep modes in the future at any speed
    // SMC_PMPROT = SMC_PMPROT_AVLP | SMC_PMPROT_ALLS | SMC_PMPROT_AVLLS;
    let smc = unsafe { &(*dev::SMC::ptr()) };
    smc.pmprot
        .write(|w| w.avlp().set_bit().alls().set_bit().avlls().set_bit());

    // teensyduino now runs equivalent of zero_bss + init_data, which
    // cortex-m-rt will do after this function finishes

    // SKIP FOR NOW:
    // default all interrupts to medium priority level
    // for (i=0; i < NVIC_NUM_INTERRUPTS + 16; i++) _VectorsRam[i] = _VectorsFlash[i];
    // for (i=0; i < NVIC_NUM_INTERRUPTS; i++) NVIC_SET_PRIORITY(i, 128);
    // SCB_VTOR = (uint32_t)_VectorsRam;	// use vector table in RAM

    //// TODO
    // initialize the SysTick counter
    // SYST_RVR = (F_CPU / 1000) - 1;
    // SYST_CVR = 0;
    // SYST_CSR = SYST_CSR_CLKSOURCE | SYST_CSR_TICKINT | SYST_CSR_ENABLE;
    // SCB_SHPR3 = 0x20200000;  // Systick = priority 32

    //// TODO (or already done by cortex-m-rt)
    // __enable_irq();

    //// TODO
    // _init_Teensyduino_internal_();
}
